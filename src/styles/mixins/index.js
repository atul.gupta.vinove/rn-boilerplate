import { wp } from "../../utils"

const padding = ({ top, right, bottom, left}) => ({
    paddingTop: top,
    paddingRight: right,
    paddingBottom: bottom,
    paddingLeft: left
})

const margin = ({ top, right, bottom, left}) => ({
    marginTop: top,
    marginRight: right,
    marginBottom: bottom,
    marginLeft: left
})

const typoGraph = () => ({
    fontSize: wp('4%'),
})

export {
    padding,
    margin,
    typoGraph
}