import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import PublicRoutes from './public';
import PrivateRoutes from './private';

const AppNavigation = (props) => {
  const isAuth = useSelector((state) => state.userProfile.isAuth);
  return (
    <NavigationContainer>
      {isAuth && <PrivateRoutes />}
      {!isAuth && <PublicRoutes />}
    </NavigationContainer>
  );
};

export default AppNavigation;
