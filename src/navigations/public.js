import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../scenes/login';

const Stack = createStackNavigator();


const PublicRoutes = (props) => {
    return (
        <Stack.Navigator initialRouteName="login">
            <Stack.Screen
                name="login"
                component={LoginScreen}
                options={{
                    headerShown: false,
                    headerStatusBarHeight: 0
                }}
            />
        </Stack.Navigator>
    )
}


export default PublicRoutes;