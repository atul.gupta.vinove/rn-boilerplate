import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../scenes/home';

const Stack = createStackNavigator();


const PrivateRoutes = (props) => {
    return (
        <Stack.Navigator initialRouteName="home">
            <Stack.Screen
                name="home"
                component={HomeScreen}
                options={{
                    headerShown: false,
                    headerStatusBarHeight: 0
                }}
            />
        </Stack.Navigator>
    )
}


export default PrivateRoutes;