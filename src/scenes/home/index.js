import React, { Component } from "react";
import { Text, View } from "react-native";
import Wrapper from "_organisms/wrapper";


class HomeScreen extends Component {
    render() {
        return (
            <Wrapper>
                <View>
                    <Text>
                        Hello HomeScreen
                    </Text>
                </View>
            </Wrapper>
        )
    }
}

export default HomeScreen;