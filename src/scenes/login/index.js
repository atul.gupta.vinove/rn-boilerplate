import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Wrapper from '_organisms/wrapper';

class LoginScreen extends Component {
  onLogin = () => {
    alert('hey');
  };

  render() {
    return (
      <Wrapper>
        <View>
          <TouchableOpacity onPress={this.onLogin}>
            <Text>Click</Text>
          </TouchableOpacity>
        </View>
      </Wrapper>
    );
  }
}

export default LoginScreen;
