import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';



export const wp = (value) => widthPercentageToDP(value);
export const hp = (value) => heightPercentageToDP(value);

export const updateObject = (initialObject, newParams) => {
    return {
        ...initialObject,
        ...newParams
    }
}