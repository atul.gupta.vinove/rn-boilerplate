import React from 'react';
import {SafeAreaView, StatusBar, ActivityIndicator} from 'react-native';
import {useSelector} from 'react-redux';
import {COLORS} from '../../../theme';

const Wrapper = (props) => {
  const loader = useSelector((state) => state.commonReducer.isLoader);
  return (
    <SafeAreaView {...props}>
      <StatusBar {...props} />
      {loader && <ActivityIndicator size="large" color={COLORS.LOADER} />}
      {props.children}
    </SafeAreaView>
  );
};

export default Wrapper;
