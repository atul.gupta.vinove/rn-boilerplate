import {ACTION_TYPES} from '../actions/actionTypes';
import {updateObject} from '../utils';

export const initialState = {
  isAuth: false,
  isLoader: false,
};

const userProfile = (state = initialState, {type, payload}) => {
  switch (type) {
    case ACTION_TYPES.LOGIN_SUCCESS:
      return updateObject(state, {userAuth: payload});

    default:
      return {...state};
  }
};

export default userProfile;
