import {combineReducers} from 'redux';
import userProfile from './authReducer';
import commonReducer from './commonReducer';

const reducers = combineReducers({
  userProfile,
  commonReducer,
});

export default reducers;
