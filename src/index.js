/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 * USE ENV
 * import {API_URL} from "@env"
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authDispatcher from "./actions/authDispatcher";
import AppNavigation from './navigations';

class App extends Component {
  render() {
    return (
      <AppNavigation {...this.props} />
    )
  }
}

const mapStateToProps = (state) => ({
  isAuth: state.userProfile.isAuth,
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ ...authDispatcher }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
