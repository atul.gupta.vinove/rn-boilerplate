module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins:  [
    ["module:react-native-dotenv", {
      "moduleName": "@env",
      "path": ".env",
      "blacklist": null,
      "whitelist": null,
      "safe": false,
      "allowUndefined": true
    }],
    [
      "module-resolver",
      {
        "cwd": "babelrc",
        "root": ["./src"],
        "extensions": [".js", ".ios.js", ".android.js"],
        "alias": {
          "_assets": "./src/assets",
          "_mixins":"./src/styles/mixins",
          "_organisms":"./src/components/organisms",
          "_atoms":"./src/components/atoms",
          "_molecules":"./src/components/molecules",
        }
      }
    ]
  ]
};
